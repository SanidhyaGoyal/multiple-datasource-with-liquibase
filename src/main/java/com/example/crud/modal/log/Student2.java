package com.example.crud.modal.log;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
//import javax.validation.constraints.NotBlank;
//import javax.validation.constraints.Size;

@Entity
@Table(name = "studentlog")
public class Student2 {
    @Id
    private int id;

   // @NotBlank
   // @Size(min = 3, max = 100)
    private String name;

//    @Column(columnDefinition = "text")
    //private int age;

    public Student2() {
    }

    public Student2(int id, String name, int age) {
        this.id = id;
        this.name = name;
        //this.age = age;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

   /* public int getAge() {
        return age;
    }
*/
    /*public void setAge(int age) {
        this.age = age;
    }*/
}



/*databaseChangeLog:
  - changeSet:
      id: jacuzzi
      author: Jacuzzi
      changes:
        - include:
            path: changelog/changes/create-table-student.yaml*/

/*databaseChangeLog:
  - changeSet:
      id: log
      author: Log
      changes:
        - include:
            path: changelog/changes/create-table-studentlog.yaml*/