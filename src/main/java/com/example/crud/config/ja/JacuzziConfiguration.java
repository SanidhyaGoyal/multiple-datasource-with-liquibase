package com.example.crud.config.ja;


import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import org.springframework.context.annotation.Primary;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;




import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import org.springframework.transaction.annotation.EnableTransactionManagement;


@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "JacuzziEntityManager",
        transactionManagerRef = "JacuzziTransactionManager",
        basePackages = { "com.example.crud.repo.ja","com.example.crud.config.ja" }//
)

class JacuzziConfiguration
{


    @Bean(name = "JacuzziDataSource")
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSource dataSource() {
        return DataSourceBuilder.create().build();
    }


    @Bean(name = "JacuzziEntityManager")
    public LocalContainerEntityManagerFactoryBean
    jacuzziEntityManager(
            EntityManagerFactoryBuilder builder,
            @Qualifier("JacuzziDataSource") DataSource dataSource
    ) {
        return
                builder
                        .dataSource(dataSource)
                        .packages("com.example.crud.modal.ja")
                        .persistenceUnit("postgres_demo")
                        .build();
    }


    @Bean(name = "JacuzziTransactionManager")
    public PlatformTransactionManager jacuzziTransactionManager(
            @Qualifier("JacuzziEntityManager") EntityManagerFactory entityManagerFactory
    ) {
        return new JpaTransactionManager(entityManagerFactory);
    }



}