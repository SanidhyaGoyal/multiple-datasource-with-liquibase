package com.example.crud.controller;

import com.example.crud.modal.ja.Student;
import com.example.crud.modal.log.Student2;
import com.example.crud.repo.log.StudentRepository2;
import com.example.crud.repo.ja.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/")
public class StudentController {

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private StudentRepository2 studentRepository2;

    @GetMapping("students")
    public List<Student> getStudents(){
        return  studentRepository.findAll();
    }

    @GetMapping("studentsfromlog")
    public List<Student2> getStudentsfromlog(){
        return  studentRepository2.findAll();
    }

    @PostMapping("insert")
    public Student createStudent(@RequestBody Student student){
        return studentRepository.save(student);
    }

    @GetMapping("students/{id}")
    public Optional<Student> getStudentById(@PathVariable(value = "id") int id){
        return  studentRepository.findById(id);

    }

    @PutMapping("students/{id}")
    public Student updateStudent(@PathVariable(value="id") int id, @RequestBody Student studentDetails) throws Exception {
        Student student = studentRepository.findById(id).orElseThrow(() -> new Exception());

        student.setId(studentDetails.getId());
        student.setAge(studentDetails.getAge());
        student.setName(studentDetails.getName());

        Student updateStudent = studentRepository.save(student);
        return updateStudent;
    }
    @DeleteMapping("/students/{id}")
    public ResponseEntity<?> deleteStudent(@PathVariable(value = "id") int id) throws Exception {
        Student student= studentRepository.findById(id).orElseThrow(() -> new Exception());
        studentRepository.delete(student);

        return ResponseEntity.ok().build();
    }

}