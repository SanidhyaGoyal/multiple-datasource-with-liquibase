package com.example.crud.repo.log;

import com.example.crud.modal.log.Student2;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository2 extends JpaRepository<Student2, Integer> {

}
